class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :device_id
      t.string :user_id
      t.integer :channel_id
      t.boolean :root, :default => false
      t.integer :application_id

      t.timestamps
    end
  end
end

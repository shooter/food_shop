module ApplicationHelper
  def side_nav_with_li(text, class_name, url)
    content_tag(:li, side_nav_without_li(text, class_name, url))
  end

  def side_nav_without_li(text, class_name, url)
    li_class = 'icon20 ' + class_name
    link_to(content_tag(:span, content_tag(:i, nil, class: li_class), class:'icon') + content_tag(:span, text, class: 'txt'), url)
  end
end

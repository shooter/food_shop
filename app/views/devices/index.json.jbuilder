json.array!(@devices) do |device|
  json.extract! device, :device_id, :user_id, :channel_id, :root, :application_id
  json.url device_url(device, format: :json)
end
